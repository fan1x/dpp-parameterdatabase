﻿using System;
using NUnit.Framework;
using System.Collections;
using ParameterDatabase.Exceptions;
using System.Collections.Generic;

namespace ParameterDatabase
{



    [TestFixture]
    public class LibraryTest
    {
        /// <summary>
        /// Configuration of specification for test
        /// </summary>
        public static Specification specification;

        
            [SetUp]
            public void SetUp()
            {
                specification = Creator.CreateSpecification();
                specification.SetCommonOptionSeparator("--");

                var bob = Creator.CreateOptionBuilder<BoolOption, bool>();
                bob.AddOption(Creator.CreateOptionName("verbose", true))
                    .AddOption(Creator.CreateOptionName("v", false))
                    .SetPermissionType(PermissionType.Required);
                var opt1 = bob.MakeOption();
                specification.AddOption(opt1);

                OptionBuilder<IntOption, int> iob = Creator.CreateOptionBuilder<IntOption, int>();
                iob.AddOption(Creator.CreateOptionName("size", true))
                    .AddOption(Creator.CreateOptionName("s", false))
                    .SetPermissionType(PermissionType.Optional)
                    .SetParameter(ParameterFactory.MakeIntParameter(42, PermissionType.Required));
                Option<IntOption, int> opt2 = iob.MakeOption();
                specification.AddOption(opt2);

                iob = Creator.CreateOptionBuilder<IntOption, int>();
                iob.AddOption(Creator.CreateOptionName("r", false))
                    .SetPermissionType(PermissionType.Optional)
                    .SetHelp("help for -r")
                    .SetParameter(ParameterFactory.MakeIntParameter(0, 100, PermissionType.Required));
                opt2 = iob.MakeOption();
                specification.AddOption(opt2);

                OptionBuilder<StringOption, string> sob = Creator.CreateOptionBuilder<StringOption, string>();
                ISet<string> enumValues = new HashSet<string> { "a", "b", "c" };
                sob.AddOption(Creator.CreateOptionName("q", false))
                    .SetPermissionType(PermissionType.Optional)
                    .SetParameter(ParameterFactory.MakeEnumStringParameter(enumValues, PermissionType.Required));
                var opt3 = sob.MakeOption();
                specification.AddOption(opt3);
            }

            
        



        /// <summary>
        /// Test of basic functionality of parser
        /// </summary>
        [Test]
        public void ParserTestBasic()
        {

            string[] line = { "-v", "-s", "50", "-r", "20" };

            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);

            IEnumerator enumerator = parsedOptions.GetOptions();
            enumerator.MoveNext();
            Option<BoolOption, bool> optionBool = (Option<BoolOption, bool>)enumerator.Current;
            string expectedName = "v";
            // assert equal on first parameter
            Assert.AreEqual(expectedName, optionBool.GetParsedName());

            // move to next parameter
            enumerator.MoveNext();
            Option<IntOption, int> optionInt = (Option<IntOption, int>)enumerator.Current;
            expectedName = "s";
            int expectedValue = 50;
            // assert equal on second parameter
            Assert.AreEqual(expectedName, optionInt.GetParsedName());
            Assert.AreEqual(expectedValue, optionInt.GetValue());

            // move to next parameter
            enumerator.MoveNext();
            optionInt = (Option<IntOption, int>)enumerator.Current;
            expectedName = "r";
            expectedValue = 20;
            // assert equal on third parameter
            Assert.AreEqual(expectedName, optionInt.GetParsedName());
            Assert.AreEqual(expectedValue, optionInt.GetValue());
            Assert.AreEqual("help for -r", optionInt.GetHelp());
            Assert.AreEqual(PermissionType.Optional, optionInt.GetPermissionType());
        }
        /// <summary>
        /// Test for parsing common option
        /// </summary>
        [Test]
        public void ParserTestCommon()
        {
            string[] line = { "-v", "alpha", "bravo" };

            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);
            IEnumerator enumerator = parsedOptions.GetOptions();
            enumerator.MoveNext();
            Option<BoolOption, bool> optionBool = (Option<BoolOption, bool>)enumerator.Current;
            string expectedName = "v";
            // assert equal on first parameter
            Assert.AreEqual(expectedName, optionBool.GetParsedName());

            // test common options
            enumerator = parsedOptions.GetCommonOptions();
            enumerator.MoveNext();
            Option<StringOption, string> optionCommon = (Option<StringOption, string>)enumerator.Current;
            string expectedValue = "alpha";
            Assert.AreEqual(expectedValue, optionCommon.GetValue());
            enumerator.MoveNext();
            optionCommon = (Option<StringOption, string>)enumerator.Current;
            expectedValue = "bravo";
            Assert.AreEqual(expectedValue, optionCommon.GetValue());
        }

        /// <summary>
        /// Test for parsing long option
        /// </summary>
        [Test]
        public void ParserTestLong()
        {
            string[] line = { "-v", "--size", "15", "--", "--zulu" };
            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);

            IEnumerator enumerator = parsedOptions.GetOptions();
            enumerator.MoveNext();
            enumerator.MoveNext(); //skip required 
            Option<IntOption, int> optionInt = (Option<IntOption, int>)enumerator.Current;
            string expectedName = "size";
            int expectedValue = 15;
            // assert equal on second parameter
            Assert.AreEqual(expectedName, optionInt.GetParsedName());
            Assert.AreEqual(expectedValue, optionInt.GetValue());

            enumerator = parsedOptions.GetCommonOptions();
            enumerator.MoveNext();
            Option<StringOption, string> optionCommon = (Option<StringOption, string>)enumerator.Current;
            string expectedCommonValue = "--zulu";
            Assert.AreEqual(expectedCommonValue, optionCommon.GetValue());
        }


        public delegate void LibraryTestDelegate();

        public bool ExceptionCatcher<T>(LibraryTestDelegate del)
        {

            bool value = false;
            try
            {
                del();
            }
            catch (Exception ex)
            {

                while (ex != null)
                {
                    if (ex is T)
                    {
                        value = true;
                        break;
                    }
                    else
                    {
                        ex = (Exception)ex.InnerException;
                    }
                }
            }
            return value;
        }

        /// <summary>
        /// Test for parsing value outside of specified range
        /// </summary>
        [Test]
        public void ParserTestOutOfRange()
        {
            LibraryTestDelegate del = new LibraryTestDelegate(RangeError);
            Assert.AreEqual(true, ExceptionCatcher<ParameterNotParsedException>(del));
        }

        public void RangeError()
        {
            string[] line = { "-v", "-r", "102" };
            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);
        }


        /// <summary>
        /// Test for parsing option that is not in specification
        /// </summary>
        [Test]
        public void ParserTestWrongOption()
        {
            LibraryTestDelegate del = new LibraryTestDelegate(WrongOptionError);
            Assert.AreEqual(true, ExceptionCatcher<OptionNotParsedException>(del));
        }

        public void WrongOptionError()
        {
            string[] line = { "-v", "-u" };
            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);
        }

        /// <summary>
        /// Test for parsing option parameter that is not defined in enum for string parameter
        /// </summary>
        [Test]
        public void ParserTestWrongParameter()
        {
            LibraryTestDelegate del = new LibraryTestDelegate(WrongOptionParameter);
            Assert.AreEqual(true, ExceptionCatcher<ParameterNotParsedException>(del));
        }

        public void WrongOptionParameter()
        {
            string[] line = { "-v", "-q", "dddd" };
            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);
        }

        /// <summary>
        /// Test for parsing option that is not in specification
        /// </summary>
        [Test]
        public void ParserTestNoRequiredOption()
        {
            LibraryTestDelegate del = new LibraryTestDelegate(NoRequiredOptionError);
            Assert.AreEqual(true, ExceptionCatcher<OptionNotParsedException>(del));
        }

        public void NoRequiredOptionError()
        {
            string[] line = { "-s", "50" };
            ParameterDatabase parsedOptions = Parser.ParseComandLine(line, specification);
        }
    }
}
