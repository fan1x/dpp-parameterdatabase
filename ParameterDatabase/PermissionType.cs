﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    /// <summary>
    /// Type of permission level for option or parameter.
    /// Each option or parameter must contain this option to 
    /// define wheter they are required, optional or none(forbidden). 
    /// </summary>
    public enum PermissionType
    {
        Required,
        Optional,
        None
    }
}
