﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    public class ParameterFactory
    {
        
        public static IntParameter MakeIntParameter(int min, int max, int value, PermissionType type)
        {
            return new IntParameterImpl(min, max, value, type);
        }

        public static IntParameter MakeIntParameter(int min, int max, PermissionType type)
        {
            return new IntParameterImpl(min, max, 0, type);
        }

        public static IntParameter MakeIntParameter(int value, PermissionType type)
        {
            return new IntParameterImpl(value, type);
        }

        public static BoolParameter MakeBoolParameter(bool value, PermissionType type)
        {
            return new BoolParameterImpl(value, type);
        }

        public static StringParameter MakeEnumStringParameter(ISet<string> enumValues, PermissionType type)
        {
            return new StringParameterImpl("", enumValues, type);
        }

        public static StringParameter MakeStringParameter(string value, PermissionType type)
        {
            return new StringParameterImpl(value, type);
        }

        public static StringParameter MakeStringParameter(PermissionType type)
        {
            return new StringParameterImpl(type);
        }
    }
}
