﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Implementations
{
    class OptionNameImpl:OptionName
    {
        public OptionNameImpl(string name, bool longOption = true):
            base(name, longOption)
        { }
        public string GetName()
        {
            return name;
        }

        public KeyValuePair<UInt16, string> GetIdentifier()
        {
            return new KeyValuePair<UInt16, string>((UInt16) (longOption ? 2 : 1), GetName());
        }
    }
}
