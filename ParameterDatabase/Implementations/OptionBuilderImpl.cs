﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Implementations
{
    /// <summary>
    /// Package with all informations about option parameter
    /// Stores its synonyms, required values, assigned values etc.
    /// </summary>
    class OptionBuilderImpl<T, K> : OptionBuilder<T, K> where T : OptionType<K>
    {
        public IEnumerator<OptionName> GetAllOptions()
        {
            return new OptionNameEnumerator<KeyValuePair<UInt16, string>>(optionNames.GetEnumerator());
        }

        /// <summary>
        /// Returns all parameters
        /// </summary>
        /// <returns></returns>
        public Parameter GetParameter()
        {
            return parameter;
        }
    }
}
