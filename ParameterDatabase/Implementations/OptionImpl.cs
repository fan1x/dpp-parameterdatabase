﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Exceptions;
using ParameterDatabase.Implementations;
using ParameterDatabase.Parameters.Implementations;

namespace ParameterDatabase.Implementations
{
    class OptionImpl<T, K> : IOptionImpl, Option<T, K> where T : OptionType<K>
    {
        protected K parsedValue = default(K);

        public OptionImpl(IEnumerator<KeyValuePair<KeyValuePair<ushort, string>, OptionName>> optionEnumerator,
            PermissionType type,
            ParameterImpl parameter,
            string help) :
            base(optionEnumerator, type, parameter, help)
        {
        }
        
        public K GetValue()
        {
            if (!IsParsed())
            {
                throw new ParameterNotParsedException(String.Format("Option {0} has no value", GetName()), null);
            }

            return parsedValue;
        }
        public override void ParseValue(string parsedString)
        {
            T instanceT = (T)Activator.CreateInstance(typeof(T));
            parsedValue = instanceT.Parse(parsedString);
            if (!parameter.Check(parsedValue))
            {
                throw new ParameterNotParsedException("Parameter check doesn't work");
            }
        }

        public override void UseDefaultValue()
        {
            if (parameter == null)
            {
                throw new ParameterNotParsedException("Can't use default value for parameter, because no parameter exists");
            }

            parsedValue = (K)parameter.GetDefaultValue();
        }
    }
}
