﻿using ParameterDatabase.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Implementations
{
    class ParameterDatabaseImpl:ParameterDatabase
    {
        private IList<IOption> options = new List<IOption>();
        private IList<IOption> commonOptions = new List<IOption>();

        internal void AddOption(IOption option)
        {
            options.Add(option);
        }
        internal void AddCommonOption(IOption option)
        {
            commonOptions.Add(option);
        }
        public override IEnumerator<IOption> GetOptions()
        {
            return options.GetEnumerator();
        }
        public override IEnumerator<IOption> GetCommonOptions()
        {
            return commonOptions.GetEnumerator();
        }

        public void SetException(ParameterDatabaseException exception)
        {
            parameterDatabaseException = exception;           
        }
        public void SetWarning(ParameterDatabaseException warning)
        {
            parameterDatabaseWarning = warning;
        }
    }
}
