﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Exceptions;

namespace ParameterDatabase.Implementations
{
    class SpecificationImpl : Specification
    {
        private IDictionary<KeyValuePair<UInt16, string>, IOptionImpl> options =
            new Dictionary<KeyValuePair<UInt16, string>, IOptionImpl>();
        private IList<IOption> commonOptions = new List<IOption>();
        private IList<string> separators = new List<string>();
        private string commonOptionSeparator = " ";
        private char optionNameValueSeparator = ' ';

        public SpecificationImpl(IOption option = null)
        {
            if (option != null)
            {
                AddOption(option);
            }
        }

        public IOption GetCommonOption()
        {
            var commonOption = Creator.CreateOptionBuilder<StringOption, string>()
                .SetParameter(ParameterFactory.MakeStringParameter(PermissionType.None))
                .AddOption(Creator.CreateOptionName("CommonOption"))
                .MakeOption();

            commonOptions.Add(commonOption);
            return commonOption;
        }

        public Specification AddOption(IOption newOption)
        {
            IOptionImpl optionImpl = (IOptionImpl)newOption;
            var optionEnum = optionImpl.GetNames();
            Exception commulativeException = null;
            while (optionEnum.MoveNext())
            {
                var optionID = ((OptionNameImpl)optionEnum.Current).GetIdentifier();
                if (options.ContainsKey(optionID))
                {
                    commulativeException = new Exception(String.Format("Option with name {0} already defined", optionID.Value),
                        new RedefiningPropertiesException());
                }
                else
                {
                    options[optionID] = (IOptionImpl)newOption;
                }
            }

            if (commulativeException != null)
            {
                throw commulativeException;
            }

            return this;
        }

        public IOption GetOption(KeyValuePair<UInt16, string> optionIdentifier)
        {
            if (!options.ContainsKey(optionIdentifier))
            {
                return null;
            }

            return options[optionIdentifier];
        }


        public void AddSeparator(string newSeparator)
        {
            separators.Add(newSeparator);
        }

        public IEnumerator<string> GetSeparators()
        {
            return separators.GetEnumerator();
        }

        internal IEnumerator<IOption> GetAllOptions()
        {
            return new IOptionEnumerator(options.GetEnumerator());
        }
        
        public Specification SetCommonOptionSeparator(string separator)
        {
            commonOptionSeparator = separator;
            return this;
        }

        internal string GetCommonOptionSeparator()
        {
            return commonOptionSeparator;
        }

        public Specification SetOptionNameValueSeparator(char separator)
        {
            optionNameValueSeparator = separator;
            return this;
        }

        internal char GetOptionNameValueSeparator()
        {
            return optionNameValueSeparator;
        }
    }
} 
