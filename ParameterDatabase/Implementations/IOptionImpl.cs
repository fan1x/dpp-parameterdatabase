﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Parameters.Implementations;

namespace ParameterDatabase.Implementations
{
    abstract class IOptionImpl:IOption
    {
        protected IEnumerator<KeyValuePair<KeyValuePair<ushort, string>, OptionName>> optionEnumerator = null;
        protected string help = null;
        protected ParameterImpl parameter = null;
        protected PermissionType permissionType = PermissionType.None;
        protected string parsedName = null;
        private PermissionType type;

        public IOptionImpl(IEnumerator<KeyValuePair<KeyValuePair<ushort, string>, OptionName>> optionEnumerator,
            PermissionType type,
            ParameterImpl parameter,
            string help)
        {
            SetPermisisonType(type);
            SetParameter(parameter);
            SetHelp(help);
            this.optionEnumerator = optionEnumerator;
        }

        public bool IsParsed()
        {
            return parsedName != null;
        }
        public string GetParsedName()
        {
            return parsedName;
        }
        public void ParseName(string parsedName)
        {
            this.parsedName = parsedName;
        }
        public PermissionType GetPermissionType()
        {
            return permissionType;
        }
        public ParameterImpl GetParameter()
        {
            return parameter;
        }

        public string GetHelp()
        {
            return help;
        }
        public bool HasParameter()
        {
            return parameter != null;
        }
        public void SetPermisisonType(PermissionType permissionType)
        {
            this.permissionType = permissionType;
        }
        public void SetParameter(ParameterImpl parameter)
        {
            this.parameter = parameter;
        }
        public void SetHelp(string help)
        {
            this.help = help;
        }

        public abstract void UseDefaultValue();
        public void UseDefaultName()
        {
            var name = (OptionNameImpl)GetName();
            parsedName = ((OptionNameImpl)GetName()).GetName();
        }

        public abstract void ParseValue(string parsedString);
        public IEnumerator<OptionName> GetNames()
        {
            optionEnumerator.Reset();
            return new OptionNameEnumerator<KeyValuePair<UInt16, string>>(optionEnumerator);
        }
        public OptionName GetName()
        {
            var optionEnum = GetNames();
            if (optionEnum.MoveNext())
            {
                return optionEnum.Current;
            }
            
            return null;
        }
    }
}
