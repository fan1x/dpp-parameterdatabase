﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Exceptions;

namespace ParameterDatabase
{
    public abstract class OptionType<T>
    {
        abstract public T Parse(string parseFrom);
        public abstract void CheckSupportedParameter(Parameter parameter);
    }
    public class BoolOption:OptionType<bool>
    {

        public override bool Parse(string parseFrom)
        {
            return bool.Parse(parseFrom);
        }

        public override void CheckSupportedParameter(Parameter parameter)
        {
            if (!(parameter is BoolParameter))
            {
                throw new UnsuportedParameterException("Parameter is not bool");
            }
        }
    }
    public class IntOption : OptionType<int>
    {
        public override int Parse(string parseFrom)
        {
            return int.Parse(parseFrom);
        }

        public override void CheckSupportedParameter(Parameter parameter)
        {
            if (!(parameter is IntParameter))
            {
                throw new UnsuportedParameterException("Parameter is not int");
            }
        }
    }
    public class StringOption : OptionType<string>
    {
        public override string Parse(string parseFrom)
        {
            return parseFrom;
        }

        public override void CheckSupportedParameter(Parameter parameter)
        {
            if (!(parameter is StringParameter))
            {
                throw new UnsuportedParameterException("Parameter is not string");
            }
        }
    }
}
