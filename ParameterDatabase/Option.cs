﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Implementations;

namespace ParameterDatabase
{
    public interface Option<T, K>:IOption where T:OptionType<K>
    {
        /// <summary>
        /// Getter for name of the option parsed from command line
        /// </summary>
        /// <returns>string with parsed option name</returns>
        string GetParsedName();

        /// <summary>
        /// Getter for value of the option value parsed from command line
        /// </summary>
        /// <returns>value of option in defined type</returns>
        K GetValue();

        /// <summary>
        /// Getter for value of the option PermissionType defined in specification
        /// </summary>
        /// <returns>type of permission for this option</returns>
        PermissionType GetPermissionType();

        /// <summary>
        /// Getter for help of this option
        /// </summary>
        /// <returns>help for this option</returns>
        string GetHelp();
    }
}
