﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    /// <summary>
    /// Stores option and its properties
    /// </summary>
    public class OptionName
    {
        protected bool longOption;
        /// <summary>
        /// Returns option name
        /// </summary>
        protected string name;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Option name</param>
        /// <param name="longParameter">Is option long version</param>
        public OptionName(string name, bool longOption = true)
        {
            SetName(name);
            SetLong(longOption);
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetLong(bool longOption)
        {
            this.longOption = longOption;
        }

        
    }
}
