﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    public interface Specification
    {
        /// <summary>
        /// Method for adding new option to specification.
        /// </summary>
        /// <param name="newOption">option created by OptionBuilder</param>
        /// <returns>updated Specification object</returns>
        Specification AddOption(IOption newOption);

        /// <summary>
        /// Method for setting string separator for separating common options in command line
        /// </summary>
        /// <param name="commonOptionSeparator">separator value</param>
        /// <returns>updated Specification object</returns>
        Specification SetCommonOptionSeparator(string commonOptionSeparator);

        /// <summary>
        /// Method for setting string separator for separating parameter value and option name.
        /// Ex. --size=15 , separator is =.
        /// </summary>
        /// <param name="separator">separator value</param>
        /// <returns>updated Specification object</returns>
        Specification SetOptionNameValueSeparator(char separator);
    }
}
