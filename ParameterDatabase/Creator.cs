﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Implementations;

namespace ParameterDatabase
{
    public class Creator
    {
        /// <summary>
        /// Creator for OptionBuilder with defined type for created option
        /// </summary>
        /// <typeparam name="T">type of option(BoolOption/IntOption/StringOption)</typeparam>
        /// <typeparam name="K">type of parameter</typeparam>
        /// <returns></returns>
        public static OptionBuilder<T, K> CreateOptionBuilder<T, K>() where T : OptionType<K>
        {
            return new OptionBuilderImpl<T, K>();
        }

        public static Specification CreateSpecification()
        {
            return new SpecificationImpl();
        }

        public static OptionName CreateOptionName(string name, bool longOption = true)
        {
            return new OptionNameImpl(name, longOption);
        }
    }
}
