﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    class IOptionEnumerator:IEnumerator<IOption>
    {
        private IEnumerator<KeyValuePair<KeyValuePair<ushort, string>, Implementations.IOptionImpl>> enumerator;

        public IOptionEnumerator(IEnumerator<KeyValuePair<KeyValuePair<ushort, string>, Implementations.IOptionImpl>> enumerator)
        {
            this.enumerator = enumerator;
        }
        public IOption Current
        {
            get { return (IOption)enumerator.Current.Value; }
        }

        public void Dispose()
        {
            enumerator.Dispose();
        }

        object System.Collections.IEnumerator.Current
        {
            get { return this.Current; }
        }

        public bool MoveNext()
        {
            return enumerator.MoveNext();
        }

        public void Reset()
        {
            enumerator.Reset();
        }
    }
}
