﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Exceptions
{
    public class ParameterDatabaseException : Exception
    {
        public ParameterDatabaseException() { }
        public ParameterDatabaseException(string message) : base(message) { }
        public ParameterDatabaseException(string message, Exception inner) : base(message, inner) { }
        protected ParameterDatabaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
