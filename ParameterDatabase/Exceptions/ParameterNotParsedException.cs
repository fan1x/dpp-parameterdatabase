﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Exceptions
{
    public class ParameterNotParsedException:ParameterDatabaseException
    {
        public ParameterNotParsedException() { }
        public ParameterNotParsedException(string message) : base(message) { }
        public ParameterNotParsedException(string message, Exception inner) : base(message, inner) { }
        protected ParameterNotParsedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
