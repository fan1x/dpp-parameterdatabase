﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Exceptions
{
    public class OptionNotParsedException:ParameterDatabaseException
    {
        public OptionNotParsedException() { }
        public OptionNotParsedException(string message) : base(message) { }
        public OptionNotParsedException(string message, Exception inner) : base(message, inner) { }
        protected OptionNotParsedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
