﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Exceptions
{
    public class UnsuportedParameterException:ParameterDatabaseException
    {
        public UnsuportedParameterException() { }
        public UnsuportedParameterException(string message) : base(message) { }
        public UnsuportedParameterException(string message, Exception inner) : base(message, inner) { }
        protected UnsuportedParameterException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
