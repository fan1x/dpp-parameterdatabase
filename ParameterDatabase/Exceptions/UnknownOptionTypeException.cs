﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Exceptions
{
    public class UnknownOptionTypeException:ParameterDatabaseException
    {
        public UnknownOptionTypeException() { }
        public UnknownOptionTypeException(string message) : base(message) { }
        public UnknownOptionTypeException(string message, Exception inner) : base(message, inner) { }
        protected UnknownOptionTypeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
