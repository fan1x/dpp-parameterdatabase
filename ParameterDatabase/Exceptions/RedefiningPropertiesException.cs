﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Exceptions
{
    public class RedefiningPropertiesException:ParameterDatabaseException
    {
        public RedefiningPropertiesException() { }
        public RedefiningPropertiesException(string message) : base(message) { }
        public RedefiningPropertiesException(string message, Exception inner) : base(message, inner) { }
        protected RedefiningPropertiesException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
