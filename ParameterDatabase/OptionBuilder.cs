﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Exceptions;
using ParameterDatabase.Implementations;
using ParameterDatabase.Parameters.Implementations;

namespace ParameterDatabase
{
    public abstract class OptionBuilder<T, K> where T:OptionType<K>
    {
        protected Parameter parameter = null;
        protected PermissionType permissionType = PermissionType.Required;
        protected string help = null;
        protected IDictionary<KeyValuePair<UInt16, string>, OptionName> optionNames =
            new Dictionary<KeyValuePair<UInt16, string>, OptionName>();

        const UInt16 LONG_PARAMETER = 2;
        const UInt16 SHORT_PARAMETER = 1;
        const UInt16 COMMON_PARAMETER = 0;

        /// <summary>
        /// Method for adding new option name or synonym
        /// </summary>
        /// <param name="newOption">new OptionName created by Creator</param>
        /// <returns>updated OptionBuilder</returns>
        public OptionBuilder<T, K> AddOption(OptionName newOption)
        {
            OptionNameImpl optionImpl = (OptionNameImpl)newOption;
            if (optionNames.ContainsKey(optionImpl.GetIdentifier()))
            {
                throw new RedefiningPropertiesException();
            }

            optionNames[optionImpl.GetIdentifier()] = optionImpl;
            return this;
        }

        /// <summary>
        /// Method for adding help to this option
        /// </summary>
        /// <param name="newOption">help</param>
        /// <returns>updated OptionBuilder</returns>
        public OptionBuilder<T, K> SetHelp(string help)
        {
            if (this.help != null)
            {
                throw new RedefiningPropertiesException();
            }

            this.help = help;
            return this;
        }

        /// <summary>
        /// Method for adding permission to this option
        /// </summary>
        /// <param name="newOption">PermissionType type</param>
        /// <returns>updated OptionBuilder</returns>
        public OptionBuilder<T, K> SetPermissionType(PermissionType permission)
        {
            permissionType = permission;
            return this;
        }

        /// <summary>
        /// Method for adding Parameter to this option
        /// </summary>
        /// <param name="newOption">new parameter</param>
        /// <returns>updated OptionBuilder</returns>
        public OptionBuilder<T, K> SetParameter(Parameter newParameter)
        {
            T instanceT = (T)Activator.CreateInstance(typeof(T));
            instanceT.CheckSupportedParameter(newParameter);

            if (parameter != null)
            {
                throw new RedefiningPropertiesException();
            }

            parameter = newParameter;
            return this;
        }
        /// <summary>
        /// Method for finalizing new option after setting all desired features.
        /// </summary>
        /// <returns>new option object with all defined features</returns>
        public Option<T, K> MakeOption()
        {
            return new OptionImpl<T, K>(optionNames.GetEnumerator(), permissionType, (ParameterImpl)parameter, help);
        }
    }
}
