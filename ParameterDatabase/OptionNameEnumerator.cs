﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Implementations;

namespace ParameterDatabase
{

    public class OptionNameEnumerator<TKey> : IEnumerator<OptionName>
    {
        private IEnumerator<System.Collections.Generic.KeyValuePair<TKey, OptionName>> enumerator;
        public OptionNameEnumerator(IEnumerator<System.Collections.Generic.KeyValuePair<TKey, OptionName>> enumerator)
        {
            this.enumerator = enumerator;
        }

        OptionName IEnumerator<OptionName>.Current
        {
            get 
            {
                return enumerator.Current.Value;
            }
        }

        public void Dispose()
        {
            enumerator.Dispose();
        }

        object System.Collections.IEnumerator.Current
        {
            get 
            {
                return enumerator.Current.Value;
            }
        }

        public bool MoveNext()
        {
            return enumerator.MoveNext();
        }

        public void Reset()
        {
            enumerator.Reset();
        }
    }
}
