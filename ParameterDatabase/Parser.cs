﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Implementations;
using System.Diagnostics;
using ParameterDatabase.Exceptions;

namespace ParameterDatabase
{
    public class Parser
    {
        private static UInt16 argumentName(string argument, out string argName)
        {
            Debug.Assert(argument.Length >= 0);
            if (argument.Length == 0 || argument[0] != '-')
            {
                argName = argument;
                return 0;
            }

            return (UInt16)(1 + argumentName(argument.Substring(1), out argName));
        }

        private static void CheckOptions(SpecificationImpl specification)
        {
            var optionEnumerator = specification.GetAllOptions();
            ParameterDatabaseException commulativeException = null;
            while(optionEnumerator.MoveNext())
            {
                IOptionImpl option = (IOptionImpl)optionEnumerator.Current;
                if (!option.IsParsed())
                {
                    if (option.GetPermissionType() == PermissionType.Required)
                    {
                        commulativeException = new OptionNotParsedException(
                            String.Format("Option {0} was not found", option.GetName()),
                            commulativeException);
                    }
                    else if (option.GetPermissionType() == PermissionType.Optional)
                    {
                        option.UseDefaultName();
                        option.UseDefaultValue();
                    }
                }
            }
            
            if (commulativeException != null)
            {
                throw commulativeException;
            }
        }
        /// <summary>
        /// Method for parsing options defined in command line.
        /// </summary>
        /// <param name="args">command line with options</param>
        /// <param name="specification">specification with defined options that are acceptable by program</param>
        /// <returns>database with parsed options</returns>
        public static ParameterDatabase ParseComandLine(string[] args, Specification specification)
        {
            SpecificationImpl spec = (SpecificationImpl)specification;
            ParameterDatabaseImpl paramDatabase = new ParameterDatabaseImpl();
            
            ParameterDatabaseException commulativeException = null;
            ParameterDatabaseException commulativeWarnings = null;
            
            bool optionalParameter = false;
            bool requiredParameter = false;
            bool commonParameters = false;

            IOptionImpl lastOption = null;
            int i = 0;
            while (i < args.Length)
            {
                try
                {
                    if (args[i].Equals(spec.GetCommonOptionSeparator()))
                    {
                        commonParameters = true;
                        optionalParameter = false;
                        requiredParameter = false;

                        ++i;
                        continue;
                    }

                    string strippedArgName = null;
                    UInt16 dashCount = argumentName(args[i], out strippedArgName);

                    if (requiredParameter)
                    {
                        if (dashCount != 0)
                        {
                            commulativeWarnings = new ParameterDatabaseException(
                                String.Format("Parameter of option {0} starts with dashes", lastOption.GetParsedName()),
                                commulativeWarnings);
                        }

                        Debug.Assert(lastOption != null);
                        // have to take args[i] because strippedArgName has no dashes
                        lastOption.ParseValue(args[i]);
                        lastOption = null;
                        requiredParameter = false;
                    }
                    // optional parameter exists
                    else if (optionalParameter && dashCount == 0)
                    {
                        Debug.Assert(lastOption != null);
                        lastOption.ParseValue(args[i]);
                        lastOption = null;
                        optionalParameter = false;
                    }
                    // common option
                    else if (commonParameters || dashCount == 0)
                    {
                        IOptionImpl commonOption = (IOptionImpl)spec.GetCommonOption();
                        commonOption.UseDefaultName();
                        commonOption.ParseValue(args[i]);
                        paramDatabase.AddCommonOption(commonOption);
                    }
                    // only option
                    else
                    {
                        // optional parameter doesn't exists
                        if (optionalParameter)
                        {
                            lastOption.UseDefaultValue();
                            lastOption = null;
                            requiredParameter = false;
                            optionalParameter = false;
                        }


                        char separator = spec.GetOptionNameValueSeparator();
                        if (separator != ' ')
                        {
                            int separatorIndex = strippedArgName.IndexOf(separator);
                            if (separatorIndex == -1)
                            {
                                throw new OptionNotParsedException(
                                    String.Format("Option {0} doesn't contains separator {1}", strippedArgName, separator));
                            }

                            string optionName = strippedArgName.Substring(0, separatorIndex);
                            string optionValue = strippedArgName.Substring(separatorIndex+1);

                            // dash count stays the same
                            IOptionImpl option = (IOptionImpl)spec.GetOption(new KeyValuePair<UInt16, string>(dashCount, optionName));
                            if (option == null)
                            {
                                throw new OptionNotParsedException(
                                    String.Format("Option with name {0} doesn't exists", optionName));
                            }

                            option.ParseName(optionName);
                            option.ParseValue(optionValue);
                            paramDatabase.AddOption(option);
                            ++i;
                            continue;
                        }
                        
                        IOptionImpl optionImpl = (IOptionImpl)spec.GetOption(new KeyValuePair<UInt16, string>(dashCount, strippedArgName));
                        if (optionImpl == null)
                        {
                            throw new OptionNotParsedException(
                                String.Format("Option with name {0} doesn't exists", strippedArgName));
                        }

                        optionImpl.ParseName(strippedArgName);
                        if (optionImpl.HasParameter())
                        {
                            optionalParameter = optionImpl.GetParameter().GetPermission() == PermissionType.Optional;
                            requiredParameter = optionImpl.GetParameter().GetPermission() == PermissionType.Required;
                            lastOption = optionImpl;
                        }
                        paramDatabase.AddOption(optionImpl);
                    }
                }
                catch (Exception ex)
                {
                    commulativeException = new ParameterDatabaseException("There were problem during parsing parameter", ex);
                }
                
                ++i;
            }

            try
            {
                CheckOptions(spec);
            }
            catch(OptionNotParsedException ex)
            {
                commulativeException = new ParameterDatabaseException("Some options were not parsed", ex);
            }

            if (commulativeException != null)
            {
                throw commulativeException;
            }

            return paramDatabase;
        }
    }
}
