﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string[] line = { "-ver", "--size", "15", "--", "--zulu" };
            args = line;

            Specification specification = Creator.CreateSpecification();
            specification.SetCommonOptionSeparator("--");
            //specification.SetOptionNameValueSeparator('=');

            var bob = Creator.CreateOptionBuilder<BoolOption, bool>();
            bob.AddOption(Creator.CreateOptionName("verbose", true))
                .AddOption(Creator.CreateOptionName("v", false))
                .SetPermissionType(PermissionType.Optional)
                .SetParameter(ParameterFactory.MakeBoolParameter(false, PermissionType.None));
            var opt1 = bob.MakeOption();
            specification.AddOption(opt1);
            
            OptionBuilder<IntOption, int> iob = Creator.CreateOptionBuilder<IntOption, int>();
            iob.AddOption(Creator.CreateOptionName("size", true))
                .AddOption(Creator.CreateOptionName("s", false))
                .SetPermissionType(PermissionType.Optional)
                .SetParameter(ParameterFactory.MakeIntParameter(15, 30, PermissionType.Required)); 
            Option<IntOption, int> opt2 = iob.MakeOption();
            specification.AddOption(opt2);            

            //string[] line = { "-v", "true", "-s", "50" };
            args = line;
            ParameterDatabase parsedOptions = Parser.ParseComandLine(args, specification);

            bool s1 = opt1.GetValue();
            var s2 = opt1.GetParsedName();
            var s3 = opt1.GetPermissionType();
            var s4 = opt1.GetHelp();
            int ival = opt2.GetValue();

            var ce = parsedOptions.GetCommonOptions();
            while(ce.MoveNext())
            {
                Option<StringOption, string> op = (Option<StringOption, string>)ce.Current;
                string str = op.GetValue();
            }

            var e = parsedOptions.GetOptions();
            while(e.MoveNext())
            {
                IOption op = e.Current;
            }
        }
    }
}
