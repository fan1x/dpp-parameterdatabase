﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    /// <summary>
    /// General interface fo all parameters
    /// </summary>
    public interface Parameter
    {
        void SetPermission(PermissionType permission);
    }
}
