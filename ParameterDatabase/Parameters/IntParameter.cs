﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    public interface IntParameter: Parameter
    {
        void SetMax(int maxValue);
        void SetMin(int minValue);
        void SetDefaultValue(int defaultValue);
    }
}
