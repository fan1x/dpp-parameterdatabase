﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    public interface StringParameter : Parameter
    {
        void SetMaxLength(uint stringMaxLength);
        void AddPossibleValue(string possibleValue);
        void SetDefaultValue(string defaultValue);
    }
}
