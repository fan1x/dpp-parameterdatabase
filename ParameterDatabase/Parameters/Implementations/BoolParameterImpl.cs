﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParameterDatabase.Parameters.Implementations;


namespace ParameterDatabase
{
    class BoolParameterImpl : ParameterImpl, BoolParameter 
    {
        protected bool defaultValue;

        public BoolParameterImpl(bool defaultValue, PermissionType permission):
            base(permission)
        {
            SetDefaultValue(defaultValue);
        }

        public void SetDefaultValue(bool defaultValue)
        {
            this.defaultValue = defaultValue;
        }

        public override object GetDefaultValue()
        {
            return defaultValue;
        }

        public override bool Check(object parsedValue)
        {
            return true;
        }
    }
}
