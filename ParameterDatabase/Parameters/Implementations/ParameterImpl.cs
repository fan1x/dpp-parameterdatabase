﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase.Parameters.Implementations
{
    abstract class ParameterImpl:Parameter
    {
        protected PermissionType permissionType = PermissionType.None;
        public ParameterImpl(PermissionType permission)
        {
            permissionType = permission;
        }

        public PermissionType GetPermission()
        {
            return permissionType;
        }
        public void SetPermission(PermissionType permission)
        {
            this.permissionType = permission;
        }

        public abstract object GetDefaultValue();
        public abstract bool Check(object parsedValue);
    }
}
