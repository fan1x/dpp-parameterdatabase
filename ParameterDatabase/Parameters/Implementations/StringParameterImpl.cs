﻿using ParameterDatabase.Parameters.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    class StringParameterImpl : ParameterImpl, StringParameter
    {
        protected string defaultValue = null;
        protected uint maxLength = uint.MaxValue;
        protected ISet<string> possibleValues = new HashSet<string>();
        private PermissionType type;

        public StringParameterImpl(string defaultValue, ISet<string> values,  PermissionType type):
            base(type)
        {
            SetDefaultValue(defaultValue);
            this.possibleValues = values;         
        }

        public StringParameterImpl(string defaultValue, PermissionType type) :
            this(defaultValue, null, type)
        {
        }

        public StringParameterImpl(PermissionType type):
            base(type)
        {
        }

        public override object GetDefaultValue()
        {
            return defaultValue;
        }

        public IEnumerator<string> GetPossibleValues()
        {
            return possibleValues.GetEnumerator();
        }
        public void SetMaxLength(uint stringMaxLength)
        {
            this.maxLength = stringMaxLength;
        }
        public void AddPossibleValue(string possibleValue)
        {
            possibleValues.Add(possibleValue);
        }
        public void SetDefaultValue(string defaultValue)
        {
            this.defaultValue = defaultValue;
        }
        public override bool Check(object parsedValue)
        {
            string pv = (string)parsedValue;
            if (possibleValues.Count > 0)
            {
                if (!possibleValues.Contains(parsedValue))
                {
                    return false;
                }
            }
            
            return pv.Length <= maxLength;
        }
    }
}
