﻿using ParameterDatabase.Parameters.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    class IntParameterImpl : ParameterImpl, IntParameter
    {
        protected int minValue;
        protected int maxValue;
        protected int defaultValue;

        public IntParameterImpl(int minValue = int.MinValue, int maxValue = int.MaxValue, int defaultValue = default(int), PermissionType type = PermissionType.Required):
            base(type)
        {
            SetMin(minValue);
            SetMax(maxValue);
            SetDefaultValue(defaultValue);
        }

        public IntParameterImpl(int defaultValue, PermissionType permissionType) :
            this(int.MinValue, int.MaxValue, defaultValue, permissionType)
        { }
        public override object GetDefaultValue()
        {
            return defaultValue;
        }

        public bool IsInRange(int compared)
        {
            return (compared >= minValue) && (compared <= maxValue);
        }
        public void SetMax(int maxValue)
        {
            this.maxValue = maxValue;
        }
        public void SetMin(int minValue)
        {
            this.minValue = minValue;
        }
        public void SetDefaultValue(int defaultValue)
        {
            this.defaultValue = defaultValue;
        }

        public override bool Check(object parsedValue)
        {
            int pv = (int)parsedValue;
            return minValue <= pv && pv <= maxValue;
        }
    }
}
