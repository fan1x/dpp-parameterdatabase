﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParameterDatabase
{
    public interface BoolParameter : Parameter
    {
        void SetDefaultValue(bool defaultValue);
    }
}