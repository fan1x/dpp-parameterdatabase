﻿using ParameterDatabase.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterDatabase
{
    public abstract class ParameterDatabase
    {
        protected ParameterDatabaseException parameterDatabaseException = null;
        protected ParameterDatabaseException parameterDatabaseWarning = null;

        /// <summary>
        /// Getter for all options that were parsed by Parser
        /// </summary>
        /// <returns>parsed options</returns>
        public abstract IEnumerator<IOption> GetOptions();
        
        /// <summary>
        /// Getter for all common options that were parsed by Parser
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerator<IOption> GetCommonOptions();
        
        /// <summary>
        /// Getter for exceptions occured during parsing
        /// </summary>
        /// <returns>list of exceptions</returns>
        public ParameterDatabaseException GetExceptions()
        {
            return parameterDatabaseException;
        }

        public ParameterDatabaseException GetWarnings()
        {
            return parameterDatabaseWarning;
        }
    }
}
