SLNFILE=ParameterDatabase.sln
LIBDIR=ParameterDatabase
UNITTESTDIR=LibraryTestProject/
UNITTESTDLL=LibraryTestProject.dll
NUNIT=nunit-console

all: rebuild test

rebuild: clean build

build:
	xbuild $(SLNFILE)
clean:
	rm -rf $(LIBDIR)/bin
	rm -rf $(LIBDIR)/obj
	rm -rf $(UNITTESTDIR)/bin
	rm -rf $(UNITTESTDIR)/obj
test:
	$(NUNIT) $(UNITTESTDIR)/bin/Debug/$(UNITTESTDLL)
